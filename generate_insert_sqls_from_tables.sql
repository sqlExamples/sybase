--@author akadir - 02/02/2018
declare @tableNames varchar(1500), @whereClauses varchar(1000), @whereClause varchar(200), @tableName varchar(50), @tableNameWithPrefix varchar(200)
declare @columns varchar(500), @columnValues varchar(1000)
declare @insertStatement varchar(1000), @executeStatement varchar(5000)
declare @piece varchar(200), @pos tinyint, @iterator tinyint, @maxColumnId tinyint, @columnPos tinyint, @tablePos tinyint, @maxTableId tinyint
declare @tableCount tinyint, @whereCount tinyint, @isDebug tinyint, @delimiter char(1)

set @delimiter = '|'

set @tableNames = "TABLE_A | TABLE_B"
--should start with "where" or be empty
set @whereClauses = "where COLUMN_A_VALUE IN (3,17,16,18)"

set @isDebug = 0

create table #tmpTableNames(id numeric(7,0) identity, tableName varchar(500))
create table #tmpWhereClauses(id numeric(7,0) identity, whereClause varchar(500))
create table #tmpColumnNames(id numeric(7,0) identity, tableId int, columnName varchar(500), columnType varchar(100))

/*-----------------------------------------------------------PARSE TABLE NAMES---------------------------------------------------------------------------------*/
set @pos = charindex(@delimiter, @tableNames)
while @pos <> 0
begin
    set @piece = left(@tableNames, @pos-1)
    if(@piece != '')
        if(charindex(".", @piece) != 0)
        begin
            select "Tablo adnda veri taban prefixi ya da nokta gibi karakterler bulunmamal" as error, ltrim(rtrim(@piece)) as "tablo ad"
            goto finish
        end
            insert into #tmpTableNames(tableName) values (ltrim(rtrim(@piece)))
    set @tableNames = stuff(@tableNames, 1, @pos, null)
    set @pos = charindex(@delimiter, @tableNames)
end

if(@tableNames != '')
begin
    if(charindex(".", @tableNames) != 0)
    begin
        select "Tablo adnda veri taban prefixi ya da nokta gibi karakterler bulunmamal" as error, ltrim(rtrim(@tableNames)) as "tablo ad"
        goto finish
    end
    insert into #tmpTableNames (tableName) values (ltrim(rtrim(@tableNames)))
end
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------PARSE WHERE CLAUSES-------------------------------------------------------------------------------*/
set @pos = charindex(@delimiter, @whereClauses)
while @pos <> 0
begin
    set @piece = left(@whereClauses, @pos-1)
    if(@piece != '')
        if(charindex("where", lower(@piece)) = 0)
        begin
            select "@whereClause deeri 'where' ile balamal ya da bo olmal" as error, @piece as "where clause"
            goto finish
        end
        insert into #tmpWhereClauses(whereClause) values (ltrim(rtrim(@piece)))
    set @whereClauses = stuff(@whereClauses, 1, @pos, null)
    set @pos = charindex(@delimiter, @whereClauses)
end

if(@whereClauses != '')
begin
    if(charindex("where", lower(@whereClauses)) = 0)
    begin
        select "@whereClause deeri 'where' ile balamal ya da bo olmal" as error, @whereClauses as "where clause"
        goto finish
    end
    insert into #tmpWhereClauses(whereClause) values (ltrim(rtrim(@whereClauses)))
end
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------CHECK TABLE AND WHERE CLAUSE COUNTS-----------------------------------------------------------------------*/
select @tableCount = count(*) from #tmpTableNames
select @whereCount = count(*) from #tmpWhereClauses

if(@tableCount != @whereCount and @whereCount not in (0,1))
begin
    select "tablo says ile where clause says uyumuyor" as error, @tableCount as "table count", @whereCount as "where count"
    goto finish
end

if(@isDebug = 1)
begin
    select id, tableName as "table name(s) and where clause(s)" from #tmpTableNames
    union all
    select id, whereClause as "table name(s) and where clause(s)" from #tmpWhereClauses
    select @tableCount as "table count", @whereCount as "where count"
end
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*----------------------------------------------------GET COLUMN NAMES FOR EACH TABLE--------------------------------------------------------------------------*/
select @iterator = min(id) from #tmpTableNames
while(@iterator <= @tableCount)
begin
    select @tableName = tableName from #tmpTableNames where id = @iterator
    insert into #tmpColumnNames select @iterator, c.name, t.name from sysobjects o
        join syscolumns c on o.id = c.id
        join systypes t on t.type = c.type and t.usertype = c.usertype
        where o.type = "U" and o.name = @tableName
    set @iterator = @iterator+ 1
end

if(@isDebug = 1)
    select * from #tmpColumnNames
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------CREATE AND PRINT INSERT STATEMENTS--------------------------------------------------------------------*/
select @tablePos = min(id), @maxTableId = max(id) from #tmpTableNames
while @tablePos <= @maxTableId
begin
    set @columns = "", @columnValues = ""
    select @columnPos = min(id), @maxColumnId = max(id) from #tmpColumnNames where tableId = @tablePos
    select @tableNameWithPrefix = db_name() + ".." + tableName from #tmpTableNames where id = @tablePos
    while @columnPos <= @maxColumnId
    begin
        select @columns = @columns + ", " + c.columnName, @columnValues = @columnValues + ", '""+rtrim(str_replace(" + c.columnName + ",""'"",""''""))+""'"
            from #tmpColumnNames c
            where c.id = @columnPos and c.columnType in ("char", "varchar")
        select @columns = @columns + ", " + c.columnName, @columnValues = @columnValues + ", ""+convert(varchar," + c.columnName + ")+"""
            from #tmpColumnNames c
            where c.id = @columnPos and c.columnType not in ("char", "varchar")
    set @columnPos = @columnPos + 1
    end

    set @columns = rtrim(ltrim(substring(@columns, 3, len(@columns))))
    set @columnValues = rtrim(ltrim(substring(@columnValues, 3, len(@columnValues))))

    if(@isDebug = 1)
    begin
        select @tableNameWithPrefix as "table name", @columns as "column names combined"
        select @tableNameWithPrefix as "table name", @columnValues as "column values combined"
    end

    if(@whereCount > 1)
        select @whereClause = whereClause from #tmpWhereClauses where id = 1
    else
        select @whereClause = whereClause from #tmpWhereClauses where id = @tablePos

    set @insertStatement = "insert into " + @tableNameWithPrefix + "(" + @columns + ") values (" + @columnValues + ")"
    set @executeStatement = "select """ + @insertStatement  + """ as ""insert sql for "+@tableNameWithPrefix+""", * from " + @tableNameWithPrefix + " " + @whereClause

    if(@isDebug = 1)
    begin
        select @insertStatement as "insert statement"
        select @executeStatement as "execute statement"
    end

    exec(@executeStatement)

    set @tablePos = @tablePos + 1
end
/*-------------------------------------------------------------------------------------------------------------------------------------------------------------*/
finish:
drop table #tmpTableNames
drop table #tmpWhereClauses
drop table #tmpColumnNames