select o.name as "Object name",
		u.name as "User/group name",
		case 
			when p.action = 193 then "select"
			when p.action = 195 then "insert"
			when p.action = 196 then "delete"
			when p.action = 197 then "update"
			when p.action = 224 then "execute"
			else "other"
		end as "Permission"
from sysobjects o, sysprotects p, sysusers u
where o.id = p.id and p.uid = u.uid
    and (u.name = "rwgrp" or u.name = "rogrp")
order by  o.name, u.name, p.action
